package org.entor.entity;

public class Pagenum {
	
//	新闻总数
	private int newsum = 0;
	//页码
	private int pagenum = 0;
	public Pagenum(int newsum,int pagenum){
		this.newsum = newsum;
		this.pagenum = pagenum;
		
	}
	public int getNewsum() {
		return newsum;
	}
	public void setNewsum(int newsum) {
		this.newsum = newsum;
	}

	public int getPagenum() {
		return pagenum;
	}
	public void setPagenum(int pagenum) {
		
			this.pagenum = pagenum;
	}
	
}
