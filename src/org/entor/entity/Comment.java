package org.entor.entity;

import java.io.Serializable;
import java.util.Date;



public class Comment implements Serializable,Comparable<Comment>{

	private int cid;
	private int cnid;
	private String ccontent;
	private Date cdate;
	private String cip;
	private String cauthor;
	public Comment(){
    	
    }
    public Comment(int cnid, String ccontent, String cip, String cauthor,Date cdate) {
		this.cdate = cdate;
		this.cnid = cnid;
		this.ccontent = ccontent;
		this.cip = cip;
		this.cauthor = cauthor;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getCnid() {
		return cnid;
	}
	public void setCnid(int cnid) {
		this.cnid = cnid;
	}
	public String getCcontent() {
		return ccontent;
	}
	public void setCcontent(String ccontent) {
		this.ccontent = ccontent;
	}
	public Date getCdate() {
		return cdate;
	}
	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}
	public String getCip() {
		return cip;
	}
	public void setCip(String cip) {
		this.cip = cip;
	}
	public String getCauthor() {
		return cauthor;
	}
	public void setCauthor(String cauthor) {
		this.cauthor = cauthor;
	}

	public String toString() {
		return "cid=" + cid + ", cnid=" + cnid + ", ccontent=" + ccontent + ", cdate=" + cdate + ", cip=" + cip
				+ ", cauthor=" + cauthor ;
	}

	public int compareTo(Comment o) {
		// TODO Auto-generated method stub
		return cdate.after(o.getCdate())?1:-1;
	}
}
