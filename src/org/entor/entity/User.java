package org.entor.entity;

import java.io.Serializable;

public class User implements Comparable<User>,Serializable{
	private int uid;
	private String uname;
	private String upwd;
//	无参构造
	public User(){
		
	}
	public User(String uname,String upwd){
		this.uname = uname;
		this.upwd = upwd;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUpwd() {
		return upwd;
	}
	public void setUpwd(String upwd) {
		this.upwd = upwd;
	}
	@Override
	public String toString() {
		return "User [uid=" + uid + ", uname=" + uname + ", upwd=" + upwd + "]";
	}
//	自定义排序方法，使用id从小到大排序
	public int compareTo(User o) {
		return o.getUid()>uid?1:-1;
	}
}
