package com.entor.configurationfile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

//import com.sun.xml.internal.fastinfoset.sax.Properties;

/**
* @ClassName: ConfigManger
* @Description: 加载配置文件
* @author 林泽
* @date 2018年10月27日 上午10:53:23
*
*/

public class ConfigManger {

	private static Properties properties = null;
	private static InputStream in = null;
	static{
//		打开数据配置文件配置文件
		in = ConfigManger.class.getClassLoader().getResourceAsStream("db.properties");
		properties = new Properties();
		try {
			properties.load(in);
		} catch (IOException e) {
			System.out.println("打开配置文件错误");
		}finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
//	得到配置文件内容
	public static String getProperties(String key){
		return properties.getProperty(key);
	}
	public static void main(String[] args) {
		System.out.println(getProperties("username"));
		System.out.println(getProperties("password"));
		System.out.println(getProperties("driver"));
	}
}
