package com.entor.sql;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.entor.entity.Topic;

import com.entor.dao.TopicDao;
import com.entor.dbutil.Databaseutil;

public class TopicDaoimpl implements TopicDao{

//	@Override
	public List<Topic> getAllTopic() {
		String sql = "select * from topic";
		
		List<Object> list = Databaseutil.executeselectlist(sql, Topic.class);
		List<Topic> topics = new LinkedList<>();
		for(Object topic:list){
			topics.add((Topic)topic);
		}
		return topics;
	}

}
