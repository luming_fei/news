package com.entor.sql;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.entor.entity.Comment;
import org.entor.entity.News;

import com.entor.dao.CommentDao;
import com.entor.dbutil.Databaseutil;

public class CommentDaoImpl implements CommentDao{

	@Override
	public List<Comment> getCommentByNid(int id) {
		String sql ="select * from comments where cnid = ?";
		List<Object> listobject = (List<Object>)Databaseutil.executeselectlist(sql, Comment.class, id);
		List<Comment> list = new LinkedList<>();
		if(listobject==null){
			return null;
		}
		for(Object object:listobject){
			list.add((Comment)object);
		}
//		for(Comment news:list){
//			System.out.println(news);
//		}
		return list;
	}

	@Override
	public int addcomment(Comment comment) {
		
		String sql = "INSERT INTO comments (cnid,ccontent,cdate,cip,cauthor)VALUES"
				+ "(?,?,?,?,?);";
		int stat = Databaseutil.executeAUD(sql, comment.getCnid(),
				comment.getCcontent(),comment.getCdate(),comment.getCip(),comment.getCauthor());
		return stat;
	}

}
