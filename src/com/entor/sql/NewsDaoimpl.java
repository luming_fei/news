package com.entor.sql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.entor.entity.News;

import com.entor.dao.NewsDao;
import com.entor.dbutil.Databaseutil;

public class NewsDaoimpl implements NewsDao{

	@Override
	public List<News> getAllNews() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	根据新闻主题id获取新闻
	public List<News> getAllNewsById(int id) throws SQLException {
		
		List<Object> list = null;
		List<News> news =  new LinkedList<>();
		String sql = "select * from news where ntid = ?";
		
		list = Databaseutil.executeselectlist(sql, News.class, id);
		for(Object object:list){
			news.add((News)object);
		}
		
		return news;
	}

//	@Override
//	获取所有新闻数量
	public int getNewsNum(int...ntid) {
		String sql ;
		int num = 0;
		if(ntid.length==0){
			sql = "select count(1) from news";
			num = Databaseutil.selecttableline(sql);
		}else{
			sql = "select count(1) from news where ntid = ?";
			num = Databaseutil.selecttableline(sql,ntid[0]);
		}
		
		
		return num;
	}

//	@Override
//	分页查询新闻
//	LinkedList<E>
	public List<News> getPaging(int pagenum,int newnum,int...tid) {
		List<Object> list = null;
		List<News> listnew = new LinkedList<>();
		String sql = null;
//		如果没有分类信息，就按所有新闻查询，如果有分类信息，就按分类查询
		if(tid.length==0){
			
			sql = "SELECT * FROM news ORDER BY nmodifyDate DESC LIMIT ?,?;";
			list = Databaseutil.executeselectlist(sql, News.class, pagenum,newnum);

		}else{
			sql = "select * from news where ntid = ? ORDER BY nmodifyDate DESC LIMIT ?,?;";
			list = Databaseutil.executeselectlist(sql, News.class, tid[0],pagenum,newnum);
		}
		
		
		for(Object object:list){
			listnew.add((News)object);
		}
		return listnew;
	}

	@Override
//	根据id获取新闻
	public News getNewsById(int nid) {
		String sql = "select * from news where nid = ?";
		News news = (News)Databaseutil.executeselectsignle(sql, News.class, nid);
//		System.out.println(news);
		return news;
	}
}
