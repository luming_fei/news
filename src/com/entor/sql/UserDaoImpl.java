package com.entor.sql;



import org.entor.entity.User;

import com.entor.dao.UserDao;
import com.entor.dbutil.Databaseutil;

public class UserDaoImpl implements UserDao{

//	检查传入的用户是否存在以及密码是否正确，无误返回用户id，否则返回-1
	@Override
	public int isuser(User user) {
//		查询用户是否存在
		String sql = "select * from news_users where uname=? and upwd=?";
		User user2 = (User)Databaseutil.executeselectsignle(sql, User.class, user.getUname(),user.getUpwd());
//		System.out.println(user2);
		if(user2==null){
			return -1;
		}else{
//			System.out.println(""+2+user2);
			return user2.getUid();
		}
//		return false;
	}

	@Override
	public boolean verificationUserBycookie(User user) {
//		int uid = user.getUid();
//		String uname = user.getUname();
		String sql = "select * from news_users where uid = ? and uname = ?";
//		System.out.println(user);
		User user2 = (User)Databaseutil.executeselectsignle(sql, User.class, user.getUid(),user.getUname());
		if(user2==null){
			return false;
		}
		return true;
	}

}
