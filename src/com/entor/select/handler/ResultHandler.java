package com.entor.select.handler;

import java.sql.ResultSet;


public interface ResultHandler {

	/**
	 * @ClassName: ResultHandler
	 * @Description: 对数据库查询结果进行处理
	 * @return 返回javabean或者list集合
	 * @date 2018年10月16日 上午9:50:42
	 *
	 */
	Object handle(ResultSet rs) throws Exception;
}
