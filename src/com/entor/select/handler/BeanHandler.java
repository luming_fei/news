package com.entor.select.handler;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

/**
* @ClassName: BeanHandler
* @Description: 对数据库查询的单行结果进行处理
* @author 林泽
* @date 2018年10月16日 上午9:54:05
*
*/

public class BeanHandler implements ResultHandler{

	private Class clazzz;
	public BeanHandler(Class clazzz){
		this.clazzz=clazzz;
	}
	
	
	public Object handle(ResultSet rs) throws Exception {
		
//		如果没有查询到数据就直接返回null不进行处理
//		if(rs==null){
//			return null;
//		}
//		System.out.println("处理");
//		获取构造函数
		Constructor constructor = clazzz.getConstructor();
//		通过构造函数创建对象
		Object object = constructor.newInstance();
//		try {
//			
//			System.out.println(rs.getInt(1));
//		} catch (Exception e) {
//			System.out.println("错误");
//			// TODO: handle exception
//		}
		
		if(rs.next()){
//			获取结果集的元数据
			ResultSetMetaData metaData = rs.getMetaData();
//			获取列数
			int count = metaData.getColumnCount();
//			System.out.println(count);
			for(int i=1;i<=count;i++){
//				获取列名
				String columnCount = metaData.getColumnName(i);
				Object columnValues = rs.getObject(columnCount);
//				设置对象成员数据
//				通过反射获取对象的成员变量
				Field field = clazzz.getDeclaredField(columnCount);
//				暴力反射
				field.setAccessible(true);
//				设置属性值
				field.set(object, columnValues);
			}
		}else{
//			没有结果，返回null
//			System.out.println("没有结果");
			return null;
		}
		
		return object;
	}
}
