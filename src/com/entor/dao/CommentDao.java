package com.entor.dao;


import java.util.List;

import org.entor.entity.Comment;

/*
 * 处理评论
 */
public interface CommentDao {

//	通过id查找评论
	public List<Comment> getCommentByNid(int nid);
//	、添加评论
	public int addcomment(Comment comment);
}
