package com.entor.dao;

import org.entor.entity.User;

public interface UserDao {

//	根据用户名密码查询用户是否存在
//	存在返回用户id否则返回-1
	public int isuser(User user);
//	根据用户id和用户名查询用户
	public boolean verificationUserBycookie(User user);
}
