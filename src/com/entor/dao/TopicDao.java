package com.entor.dao;

import java.util.List;

import org.entor.entity.Topic;

public interface TopicDao {

//	获取所有分类
	public List<Topic> getAllTopic();

}
