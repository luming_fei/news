package com.entor.dao;

import java.sql.SQLException;
import java.util.List;

import org.entor.entity.News;

/*
 * 处理新闻的接口
 */

public interface NewsDao {

//	查找所有新闻
	public List<News> getAllNews() throws SQLException;
//	获取某主题下所有新闻（根据主题id）
	public List<News> getAllNewsById(int id) throws SQLException;
//	查看新闻数量
	public int getNewsNum(int...ntid);
//	分页查询新闻,可以按照分类
	public List<News> getPaging(int pagenum,int newnum,int...tid);
//	根据id获取新闻
	public News getNewsById(int nid);
}
