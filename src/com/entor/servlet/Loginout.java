package com.entor.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Loginout extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		通过false把session中登录状态设置为退出
		HttpSession session = req.getSession();
		session.setAttribute("flag", false);
//		使用时间为0秒的的cookie替换原来cookie
		Cookie cookieuid = new Cookie("uid", "");
		Cookie cookieuname = new Cookie("uname", "loginout");
//		System.out.println(stat);
//		设置cookie有效期为一天
		cookieuid.setMaxAge(0);
		cookieuid.setPath(req.getContextPath());
		cookieuname.setMaxAge(0);
		cookieuname.setPath(req.getContextPath());
		resp.addCookie(cookieuid);
		resp.addCookie(cookieuname);
//		System.out.println(req.getContextPath());
		resp.sendRedirect("/newsself/index.jsp");
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
