package com.entor.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.entor.entity.Comment;

import com.entor.dbutil.Databaseutil;
import com.entor.sql.CommentDaoImpl;

public class Add_comment extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int nid = Databaseutil.stringtoint(req.getParameter("nid"));
		String cauthor = req.getParameter("cauthor");
		String cip = req.getParameter("cip");
		String content = req.getParameter("ccontent");
		Comment comment = new Comment(nid,content,cip,cauthor,new Date());
		CommentDaoImpl cdi = new CommentDaoImpl();
		int stat = cdi.addcomment(comment);
//		System.out.println(stat);
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter pw = resp.getWriter();
		pw.println(stat);
		pw.flush();
		pw.close();
		
	}
}
