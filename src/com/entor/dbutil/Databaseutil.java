package com.entor.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.entor.entity.User;

import com.entor.configurationfile.*;
import com.entor.select.handler.BeanHandler;
import com.entor.select.handler.BeanListHandler;
import com.sun.org.apache.regexp.internal.recompile;

import sun.print.resources.serviceui;

public class Databaseutil {

	private static String driver = ConfigManger.getProperties("driver");
	private static String url = ConfigManger.getProperties("url");
	private static String username = ConfigManger.getProperties("username");
	private static String password = ConfigManger.getProperties("password");
	private static List<Connection> mysqlList = new ArrayList<>();
	private static int mysqlnumber = stringtoint(ConfigManger.getProperties("mysqlnumber"));
	//	静态块加载mysql数据库驱动
	/*
	 * 简单的数据库连接池
	 * 初始化10个数据库连接，不够的时候自动创建
	 * 使用完毕后，归还到连接池中
	 * 
	 */
	static{
		try {
			Class.forName(driver);
			for(int i=0;i<mysqlnumber;i++){
				mysqlList.add(DriverManager.getConnection(url,username,password));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/*
	 * 获取数据库连接
	 */
	private static Connection getConnection(){
		if(mysqlList.size()==0){
			for(int i=0;i<mysqlnumber;i++){
				try {
					mysqlList.add(DriverManager.getConnection(url,username,password));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		//System.out.println("当前数据库连接数量:"+mysqlList.size());
		return mysqlList.remove(mysqlList.size()-1);
	}


	//	关闭资源
	private static void closeall(ResultSet rs,PreparedStatement pstmt,Connection conn){
		try {
			if(rs!=null){
				rs.close();
			}
			if(pstmt!=null){
				pstmt.close();
			}
			if(conn!=null){
				mysqlList.add(conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * 通用的单个数据库查询方法
	 * @return 返回Object
	 */
	@SuppressWarnings("finally")
	public static Object executeselectsignle(String sql,Class clazzz,Object...param){

		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		//		处理结果的对象
		BeanHandler handler = new BeanHandler(clazzz);
		Object object = null;
//		User user = new User();
		try {
			pstmt = conn.prepareStatement(sql);
			for(int i=0;i<param.length;i++){
				pstmt.setObject(i+1, param[i]);
//				System.out.println(param[i]);
			}
			//			查询
//			如果未查询到结果，直接返回null
			rs = pstmt.executeQuery();
//			if(rs.wasNull()){
////				System.out.println("结果集");
//				closeall(rs, pstmt, conn);
//				return null;
//			}
			//			返回处理后的结果
//			System.out.println("吃力");
			object = handler.handle(rs);
//			rs.next();
//			user.setUid(rs.getInt(1));
//			user.setUname(rs.getString(2));
//			user.setUpwd(rs.getString(3));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//			如果过程出错就返回null
			//			return null;
		}finally {
//			System.out.println((User)object);
			closeall(rs, pstmt, conn);
			//			如果出错返回null
			return object;
		}

		//		return null;
	}

	/*
	 * 查询多个结果的通用方法
	 * @return List<Object>
	 */
	@SuppressWarnings("finally")
	public static List<Object> executeselectlist(String sql,Class clazzz,Object...param){

		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		//		处理结果的对象
		BeanListHandler beanListHandler = new BeanListHandler(clazzz);
		List<Object> object = null;
		try {
			pstmt = conn.prepareStatement(sql);
			for(int i = 0;i<param.length;i++){
				pstmt.setObject(i+1, param[i]);
			}
			rs = pstmt.executeQuery();
			object = beanListHandler.handle(rs);

		} catch (Exception e) {
			e.printStackTrace();

		}finally {
			//			无论是否出错，都要关闭资源
			closeall(rs, pstmt, conn);
			//			如果出错返回null
			return object;
		}
	}


	/*
	 * 通用的增删改方法
	 * 
	 */
	public static int executeAUD(String sql,Object...params){

		//		受影响的行数
		int result = 0;
		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		//		ResultSet rs = null;
		//		获取数据库连接
		//		connection = getConnection();

		try {
			//			执行sql处理
			pstmt = conn.prepareStatement(sql);
			//			设置参数
			for(int i=0;i<params.length;i++){
				pstmt.setObject(i+1, params[i]);
			}
			//			执行修改
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			//			如果报错返回-1，表示错误
			return -1;
		}finally {
			//			无论是否出错，关闭资源
			closeall(null, pstmt, conn);
		}
		return result;
	}


//	查询表行数
	public static int selecttableline(String sql,int...ntid){
		int result = 0;
		
		Connection conn = getConnection();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			if(ntid.length!=0){
				pstmt.setInt(1, ntid[0]);
			}
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			result = rs.getInt("count(1)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			closeall(null, null, conn);
		}
		return result;
	}
	
	//	字符串转换为整形
	public static int stringtoint(String string){
		char[] chars = string.toCharArray();
		int result = 0;
		for(char c:chars){
			result = result*10+(c-48);
		}
		return result;
	}

//		public static void main(String[] args) {
//			System.out.println(stringtoint("125"));
//			Connection connection = getConnection();
//			
//			try {
//				PreparedStatement preparedStatement = connection.prepareStatement("select * from news_users where uid = 1");
//				ResultSet rs = preparedStatement.executeQuery();
//				if(rs.next()){
//					System.out.println(1);
//				}else{
//					System.out.println(2);
//					if(rs.next()){
//						System.out.println(rs.getObject(1));
//					}else{
//						System.out.println(rs.next());
//					}
//				}
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
}
