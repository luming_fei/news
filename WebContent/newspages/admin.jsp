<%@page import="org.entor.util.HandleCookie"%>
<%@ page language="java" import="java.util.*,java.sql.*"
	pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>

<title>添加主题--管理后台</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jqery-1.12.4.min.js"></script>
</head>
<body>
	<div id="header">
		<div id="welcome">欢迎使用新闻管理系统！</div>
		<div id="nav">
			<div id="logo">
				<img src="../images/logo.jpg" alt="新闻中国" />
			</div>
			<div id="a_b01">
				<img src="../images/a_b01.gif" alt="" />
			</div>
		</div>
	</div>
	<div id="admin_bar">
		<div id="status">
			<%
				String name = HandleCookie.loginState(request);
				Object object = session.getAttribute("flag");
				boolean flag = false;
				if(object!=null){
					flag = (boolean)object;
				}
				//String name = (String)session.getAttribute("uname");
				if((name!="")||flag){
					%>
						管理员： <%=name %>&#160;&#160;&#160;&#160; 
						<a href="login.out">login out</a><a  href="../index.jsp">返回首页</a>
					<% 
				}else{
					response.sendRedirect("../index.jsp");
				}
			%>
		</div>
		<div id="channel"></div>
	</div>
	<div id="main">
		<%@ include file="console_element/left.html"%>
		<div id="opt_area">
			<script type="text/javascript">
    function clickdel(nid){
        if (confirm("此新闻的相关评论也将删除，确定删除吗？"))
            window.location="../util/news?opr=delete&nid="+nid;
    }
	

			<ul class="classlist">
				
			</ul>
		</div>
	</div>
	<div id="footer">
		<%@ include file="console_element/bottom.html"%>
	</div>
</body>
</html>
