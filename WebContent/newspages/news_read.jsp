<%@page import="org.entor.util.HandleCookie" %>
<%@page import="com.entor.sql.CommentDaoImpl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.entor.dbutil.Databaseutil"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>新闻中国</title>
<link href="css/read.css" rel="stylesheet" type="text/css" />
<script src="http://phsym804q.bkt.clouddn.com/jquery-1.7.2.js"></script>
<script type="text/javascript">
	  		function login(){
	  			//var cauthor = document.getElementById("cauthor");
	  			//var content = document.getElementById("ccontent");
	  			//alert($("#cauthor").val());
	  			if($("#uname").val() =="" ||$("#uname").val()==null){
	  				alert("用户名不能为空！！");
	  				
	  				return false;
	  			}else if($("#upwd").val() == ""||$("#upwd").val() == null){
	  				alert("密码不能为空！！");
	  				return false;
	  			}
	  			//alert("true");
	  			return true;
	  		}
	  	</script>
</head>
<body>
	<div id="header">
		<div id="top_login">
			
			<%
			Object object = session.getAttribute("flag");
			boolean flag = false;
			if(object!=null){
				flag = (boolean)object;
			}
				String name = HandleCookie.loginState(request);
				if(flag||!name.equals("")){
					%>
						管理员： <%=name %>&#160;&#160;&#160;&#160; 
						<a href="login.out">login out</a>&#160;&#160;&#160;<a  href="../index.jsp">返回首页</a>
					<%
				}else{
					%>
			<form action="util/login" method="post">
			<label> 登录名 </label> <input type="text" name="uname" id="uname" value=""
				class="login_input" /> <label> 密&#160;&#160;码 </label> <input
				type="password" id="upwd" name="upwd" value="" class="login_input" /> 
				<input type="submit" class="login_sub" value="登录" onclick="login()" /> <label
				id="error"> </label> <a href="../index.jsp" class="login_link">返回首页</a>
			<img src="images/friend_logo.gif" alt="Google" id="friend_logo" />
			</form>
					<%
				}
			%>
		
		</div>
		<div id="nav">
			<div id="logo">
				<img src="images/logo.jpg" alt="新闻中国" />
			</div>
			<div id="a_b01">
				<img src="images/a_b01.gif" alt="" />
			</div>
			<!--mainnav end-->
		</div>
	</div>
	<div id="container">
		<%@ include file="../index-elements/index_sidebar.jsp"%>
		<div class="main">
			<div class="class_type">
				<img src="images/class_type.gif" alt="新闻中心" />
			</div>
			<div class="content">
				<ul class="classlist">
					<table width="80%" align="center">

						<%
        	int nid = Databaseutil.stringtoint(request.getParameter("nid"));
        	//NewsDaoimpl ndi = new NewsDaoimpl();
        	News news = ndi.getNewsById(nid);
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        %>

						<tr width="100%">
							<td colspan="2" align="center"><%=news.getNtitle() %></td>
						</tr>
						<tr>
							<td colspan="2"><hr /></td>
						</tr>
						<tr>
							<td align="center">作者：<%=news.getNauthor() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td align="left">发布时间：<%=sdf.format(news.getNcreateDate()) %></td>
						</tr>
						<tr>
							<td colspan="2" align="center"></td>
						</tr>
						<tr>
							<td colspan="2"><%=news.getNcontent() %></td>
						</tr>
						<tr>
							<td colspan="2"><hr /></td>
						</tr>
					</table>
				</ul>
				<ul id="commentlist" class="classlist" type="none">
					<table id="allcomment" width="80%" align="center">
						<%
				        	CommentDaoImpl cdi = new CommentDaoImpl();
				        	List<Comment> listcComments = cdi.getCommentByNid(news.getNid());
				        	
				        	request.setAttribute("comments", listcComments);
				        %>
						<c:choose>
							<c:when test="${comments.size()==0}">
								<tr>
									<td colspan="6" id="null">暂无评论！ ${comment.getCcontent()}</td>
								</tr>

								<tr>
									<td colspan="6">

										<hr />
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${comments}" var="comment">
									<tr>
										<td>留言人：</td>
										<td>${comment.getCauthor()}</td>
										<td>IP：</td>
										<td>${comment.getCip()}</td>
										<td>留言时间：</td>
										<td><fmt:formatDate value="${comment.getCdate()}"
												pattern="yyyy-MM-dd HH:mm:ss" /></td>
									</tr>
									<tr>
										<td colspan="6">${comment.getCcontent()}</td>
									</tr>
									<tr>
										<td colspan="6"><hr /></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</table>
				</ul>
				<ul class="classlist">
					<form id="commentcontent" method="post" action="javascript:submi()">
						<input type="hidden" name="nid" value="<%=news.getNid() %>" />
						<table width="80%" align="center">
							<tr>
								<td>评 论</td>
							</tr>
							<tr>
								<td>用户名：</td>
								<td><c:choose>
										<c:when test="${not empty sessionScope.admin}">
											<input id="cauthor" name="cauthor"
												value="${sessionScope.admin}" readonly="readonly"
												style="border: 0px;" />
										</c:when>
										<c:otherwise>
											<input id="cauthor" name="cauthor" value="这家伙很懒什么也没留下" />
										</c:otherwise>
									</c:choose> IP： <input type="text" name="cip" id="cip"
									value="${pageContext.request.remoteAddr}" readonly="readonly"
									style="border: 0px;" /></td>
							</tr>
							<tr>
								<td colspan="2"><textarea name="ccontent"
										id="commentcontent1" cols="70" rows="10"></textarea></td>
							</tr>
							<tr>
								<td><input name="submit" id="submit" value="发  表"
									type="submit" /></td>
							</tr>
						</table>
					</form>
					<script>
          				$("#submit").click(function(){
          	        	  //alert(123);
          	        	  var commentcontent = $("#commentcontent1").val();
          	        	  if(commentcontent==null||commentcontent==""){
          	        		  alert("评论不能为空");
          	        		  return;
          	        	  }
          	        	  //如果没有评论，就把暂无评论的部分id设置为null，在jq通过id获取该元素时，如果长度为0，说明该元素不存在
          	        	  var nullcomment = $("#null");
          	        	  if($("#null").length>0){
          	        		  $("#allcomment").html("");
          	        	  }
          	              var comm = $("#commentcontent").serializeArray();
          	             	var c = $.param(comm);
          	              //alert(comm);
          	             // alert($("#commentcontent").serialize());
          	             // return false;
          	               $.ajax({
          	                 url:"addcomment",
          	                 dataType:"json",
          	                 type:"post",
          	                 data:comm,
          	                 success:function(date){
          	                	 if(date!='1'){
          	                		 alert("错误");
          	                		 return false;
          	                	 }
          	                	 $("#commentcontent1").val("");
          	                	 var ul = $("#commentlist");
          	                     var table = "<table width='80%' align='center'><tr><td>留言人:</td><td>"+comm[1].value+"</td>"+
          	                     "<td> IP :</td><td>"+comm[2].value+"</td>"+"<td> 留言时间:</td><td>"+gettime()+"</td></tr>"+
          	                     "<tr><td colspan='6'>"+comm[3].value+"</td></tr><tr><td colspan = '6'><hr/></td></tr>";
          	                    
          	                     ul.append(table);
          	                 }
          	               });
          	              
          	              
          	        	//  $("body").html(gettime());
          	              return false;
          	            });
              function gettime(){
		           var date = new Date();
		           return date.getFullYear()+'/'+(date.getMonth()+1)+'/'+date.getDate()+" "+date.getHours()+":"
                            +date.getMinutes()+":"+date.getSeconds();
       }
          </script>
				</ul>
			</div>
		</div>
	</div>
	<%--
    request.removeAttribute("news_view");
    request.removeAttribute("comments_view");
--%>
	<%@ include file="../index-elements/index_bottom.html"%>
</body>
</html>
