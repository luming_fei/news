<%@page import="org.entor.util.HandleCookie"%>
<%@page import="com.entor.sql.UserDaoImpl"%>
<%@page import="com.entor.dbutil.Databaseutil"%>
<%@page import="com.entor.sql.TopicDaoimpl"%>
<%@page import="com.entor.sql.NewsDaoimpl" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/main.css#" rel="stylesheet" type="text/css">
<script type="text/javascript">
	function check(){
		var login_username = document.getElementById("uname");
		var login_password = document.getElementById("upwd");
		if(login_username.value == ""){
			alert("用户名不能为空！请重新填入！");
			login_username.focus();	
			return false;
		}else if(login_password.value == ""){
			alert("密码不能为空！请重新填入！");
			login_password.focus();
			return false;
		}
		return true;
	}
	
	function focusOnLogin(){
		var login_username = document.getElementById("uname");
		login_username.focus();	
	}
</script>
</head>
<body>
	<header>
		<div id="top_login">
			<%
				Object flObject = session.getAttribute("flag");
				boolean flag = false;
				if(flObject!=null){
					flag = (boolean)flObject;
			//		if(!flag){
	//					out.print(flag+"<h1>alert('用户名或密码错误');</h1>");
		//			}
				}
				String name = HandleCookie.loginState(request);
				
				//UserDaoImpl udi = new UserDaoImpl();
				if(flag||(!name.equals(""))){
					%>
						<span>当前用户<a href="newspages/admin.jsp"><%=name %></a>
						&#160;&#160;&#160;&#160;<a href="login.out">login out</a></span>
					<%
					//out.print("<script>alert('"+flag+"');</script>");
					//out.print("<script>alert('name="+name+"');</script>");
				}else{
					%>
						<form action="util/login" method="post" onsubmit="return check()">
							<input type="hidden" name="opr" value="login" /> <label> 登录名
							</label> <input type="text" name="uname" id="uname" value=""
								class="login_input" /> <label> 密&#160;&#160;码 </label> <input
								type="password" name="upwd" id="upwd" value="" class="login_input" />
							<input type="submit" class="login_sub" value="登录" /> <label
								id="error"> </label> <img src="images/friend_logo.gif" alt="Google"
								id="friend_logo" />
						</form>					
					<%
				}
			%>
		</div>
		<div id="nav">
			<div id="logo">
				<img src="images/logo.jpg" alt="新闻中国" />
			</div>
			<div id="a_b01">
				<img src="images/a_b01.gif" alt="" />
			</div>
			<!--mainnav end-->
		</div>
	</header>
	<div id="container">

		<%@ include file="index-elements/index_sidebar.jsp"%>

		<main>
			<div class="class_type">
				<img src="images/class_type.gif" alt="新闻中心" />
			</div>
			<div class="content">
				<ul class="class_date">
					
				</ul>
				<ul class="classlist">
					<%
					//新闻分类
						TopicDaoimpl tdi = new TopicDaoimpl();
					//获取所有的新闻分类
						List<Topic> topics = tdi.getAllTopic();
						for(Topic topic:topics){
							%>
								<a href="index.jsp?rep=topic&tid=<%=topic.getTid()%>">
								<%=topic.getTname() %></a>
							<%
						}
					%>
				</ul>
				<ul class="classlist">
					<%
						//所有新闻
						//NewsDaoimpl ndi = new NewsDaoimpl();
						//获取请求方式
						String requestmethod = request.getParameter("rep");
						
						//获取浏览方式
						String browmode = (String)session.getAttribute("browmode");
						//out.print(requestmethod);
						int newsum = ndi.getNewsNum();
						List<News> newlist = null;
						int indexid = 1;
						//Pagenum pagenum = (Pagenum)session.getAttribute("pagenum");
						String indexidstr = request.getParameter("indexid");
						//if(indexidstr!=null){
						//	indexid = Databaseutil.stringtoint(indexidstr);
						//}
						//直接浏览
						if(requestmethod==null){
							newlist = ndi.getPaging((indexid-1)*10, 10);
							session.removeAttribute("browmode");
							session.removeAttribute("tid");
						}
						//通过分类进入
						else if(requestmethod.equals("topic")){
							int tid = Databaseutil.stringtoint(request.getParameter("tid"));
							newlist = ndi.getPaging((indexid-1)*10, 10, tid);
							newsum = ndi.getNewsNum(tid);
							session.setAttribute("browmode", "topic");
							session.setAttribute("tid", tid);
						}
						//跳转页数
						else if(requestmethod.equals("listtitle")){
								indexid = Databaseutil.stringtoint(indexidstr);
							if(browmode==null){
								newlist = ndi.getPaging((indexid-1)*10, 10);								
							}else if(browmode.equals("topic")){
								int tid = (Integer)session.getAttribute("tid");
								newlist = ndi.getPaging((indexid-1)*10, 10,tid);	
							}
							//out.print(indexid);
						}
						count = 1;
						if(newlist==null||newlist.size()==0){
							out.print("没有找到相关新闻");
						}else{
							for(News news:newlist){
								
								%>
									<li>
										<a href = "newspages/news_read.jsp?rep=readNew&nid=<%=news.getNid()%>"><%=news.getNtitle() %></a>
										<span><%=news.getNcreateDate() %></span>
									</li>
								<%
								if(count%4==0){
									out.print("<br/>");
								}
								count++;
							}							
						}
						
					%>
					<p align="center">当前页数:&nbsp;<%=indexid %>/<%=((newsum-1)/10+1) %>
						<%
							if(indexid>1&&indexid<((newsum-1)/10+1)){
								%>
									<a href="index.jsp?rep=listtitle&indexid=1">首页</a>
									<a href="index.jsp?rep=listtitle&indexid=<%=indexid-1%>">上一页</a>
									<a href="index.jsp?rep=listtitle&indexid=<%=indexid+1%>">下一页</a>
									<a href="index.jsp?rep=listtitle&indexid=<%=((newsum-1)/10+1)%>">末页</a>
								<%
							}else if(indexid==1&&((newsum-1)/10+1)!=1){
								%>
									
									<a href="index.jsp?rep=listtitle&indexid=2">下一页</a>
									<a href="index.jsp?rep=listtitle&indexid=<%=((newsum-1)/10+1)%>">末页</a>
								<%
							}else if(((newsum-1)/10+1)==1){
								//out.print("未查询到相关新闻");
							}else
							{
								%>
								<a href="index.jsp?rep=listtitle&indexid=1">首页</a>
								<a href="index.jsp?rep=listtitle&indexid=<%=((newsum-1)/10+1)-1%>">上一页</a>
								<%
							}
						%>
					</p>
				</ul>
			</div>
			<%@ include file="index-elements/index_rightbar.html"%>
		</main>
	</div>
	<%@ include file="index-elements/index_bottom.html"%>
</body>
</html>