<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.entor.sql.NewsDaoimpl"%>
<%@page import="org.entor.entity.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<div class="sidebar">
	<h1>
		<img src="images/title_1.gif" alt="国内新闻" />
	</h1>
	<div class="side_list">
		<ul>
			<%
      	//通过数据库查询一定数量的国内新闻，写到这里
      	NewsDaoimpl ndi = new NewsDaoimpl();
		List<News> listwithin = (List<News>)session.getAttribute("listwithin");
		List<News> list = null;
		if(listwithin==null){
			list = ndi.getAllNewsById(1);
			session.setAttribute("listwithin", list);
		}else{
			list = listwithin;
		}
		
      	int count = 0;
      	for(News news:list){
      		if(count==6){
      			break;
      		}
      		%>
			<a href="newspages/news_read.jsp?rep=readNew&nid=<%=news.getNid()%>"> <%=news.getNtitle() %></a>
			<br>
			<%
			count++;
      	}
      %>
		</ul>
	</div>
	<h1>
		<img src="images/title_2.gif" alt="国际新闻" />
	</h1>
	<div class="side_list">
		<ul>
			<%
      	//通过数据库查询一定数量的国际新闻，写到这里
      	//NewsDaoimpl ndi = new NewsDaoimpl();
		List<News> listinternation = (List<News>)session.getAttribute("listinternation");
		if(listinternation==null){
			list = ndi.getAllNewsById(2);
			session.setAttribute("listinternation", list);
		}else{
			list = listinternation;
		}
		
      	count = 0;
      	for(News news:list){
      		if(count==6){
      			break;
      		}
      		%>
			<a href="newspages/news_read.jsp?rep=readNew&nid=<%=news.getNid()%>"> <%=news.getNtitle() %></a>
			<br>
			<%
			count++;
      	}
      %>
		</ul>
	</div>
	<h1>
		<img src="images/title_3.gif" alt="娱乐新闻" />
	</h1>
	<div class="side_list">
		<ul>
			<%
      	//通过数据库查询一定数量的娱乐新闻，写到这里
      	//NewsDaoimpl ndi = new NewsDaoimpl();
		List<News> entertainmentlist = (List<News>)session.getAttribute("entertainmentlist");
		if(entertainmentlist==null){
			list = ndi.getAllNewsById(5);
			session.setAttribute("entertainmentlist", list);
		}else{
			list = entertainmentlist;
		}
      	count = 0;
      	for(News news:list){
      		if(count==6){
      			break;
      		}
      		%>
				<a href="newspages/news_read.jsp?rep=readNew&nid=<%=news.getNid()%>">
				<%=news.getNtitle() %></a><br>
			<%
			count++;
      	}
      %>
		</ul>
	</div>
</div>